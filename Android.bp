// Copyright 2006 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

cc_defaults {
    name: "bison_defaults",

    c_std: "c17",
    cflags: [
        "-Wall",
        "-Werror",
        "-Wno-unused-parameter",

        "-DEXEEXT=\"\"",
    ],

    target: {
        darwin: {
            local_include_dirs: [
                "darwin-lib",
                "lib",
            ],
        },
        glibc: {
            local_include_dirs: [
                "linux_glibc-lib",
                "lib",
            ],
        },
        musl: {
            local_include_dirs: [
                "linux_musl-lib",
                "lib",
            ],
            cflags: [
                "-Wno-error=unused-const-variable",
                "-DMUSL_LIBC",
            ],
        },
    },
}

cc_library_host_static {
    name: "libbison",
    defaults: [
        "bison_defaults",
        "libbison_linux_glibc_defaults",
        "libbison_linux_musl_defaults",
        "libbison_darwin_defaults",
    ],
}

//##########################################

cc_binary_host {
    // This isn't usable as-is from the installed directory, so name it
    // something else so it doesn't match with the prebuilt_build_tool and the
    // stem will rename it back to bison on the filesystem.
    name: "bison_bin",
    stem: "bison",
    defaults: [
        "bison_defaults",
        "bison_linux_glibc_defaults",
        "bison_linux_musl_defaults",
        "bison_darwin_defaults",
    ],

    static_libs: ["libbison"],

    target: {
        linux: {
            cflags: ["-DINSTALLDIR=\"/nonexistent/linux-x86/bin\""],
        },
        darwin: {
            cflags: ["-DINSTALLDIR=\"/nonexistent/darwin-x86/bin\""],
        },
    },
}
